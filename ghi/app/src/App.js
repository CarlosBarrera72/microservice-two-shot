import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatsList from "./HatsList";
import HatForm from "./HatForm";
import UpdateForm from "./UpdateForm";
import ShoeList from "./ShoeList";
import AddShoeForm from "./AddShoeForm";

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsList />} />
          <Route path="/hatsForm" element={<HatForm />} />
          <Route path="/update/:pk/" element={<UpdateForm />} />
          <Route path="/shoes/new/" element={<AddShoeForm />} />
          <Route path="/shoes" element={<ShoeList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
