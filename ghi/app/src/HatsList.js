import { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";

function HatsList() {
  const navigate = useNavigate();
  const [hats, setHats] = useState();
  const [selectedHat, setSelectedHat] = useState();

  useEffect(() => {
    async function loadHats() {
      try {
        const response = await fetch("http://localhost:8090/api/hats/");
        if (response.ok) {
          const data = await response.json();

          setHats(data.hats);
        } else {
          console.error(response);
        }
      } catch (error) {
        console.error("Error loading hats:", error);
      }
    }

    loadHats();
  }, []);

  if (hats === undefined) {
    return null;
  }
  const handleUpdate = (hatId) => {
    const selectedHatData = hats.find((hat) => hat.id === hatId);
    setSelectedHat(selectedHatData);
    navigate(`/update/${hatId}/`, { state: { hatData: selectedHatData } });
  };

  const handleDelete = async (hatId) => {
    const confirmDelete = window.confirm(
      "Are you sure you want to delete this hat?"
    );
    if (!confirmDelete) {
      return;
    }
    const deleteUrl = `http://localhost:8090/api/hats/${hatId}/`;
    const fetchOptions = {
      method: "DELETE",
      headers: {
        "Content-type": "application/json",
      },
    };

    try {
      const deleteResponse = await fetch(deleteUrl, fetchOptions);
      if (deleteResponse.ok) {
        setHats((prevHats) => prevHats.filter((hat) => hat.id !== hatId));
      } else {
        const errorMessage = await deleteResponse.text();
        console.error(`Error deleting hat with id ${hatId}: ${errorMessage}`);
      }
    } catch (error) {
      console.error("Error during hat deletion:", error.message);
    }
  };
  return (
    <div className="container" style={{ marginTop: "20px" }}>
      <div
        className="d-flex justify-content-end mt-3"
        style={{ marginTop: "20px", marginBottom: "20px" }}
      >
        <Link to="/hatsForm" className="btn btn-success">
          Add Hat
        </Link>
      </div>
      <div className="row">
        {hats.map((hat) => (
          <div key={hat.id} className="col-md-4 mb-4">
            <div className="card shadow" style={{ width: "18rem" }}>
              <img src={hat.picture_url} className="card-img-top" alt="Hat" />
              <div className="card-body">
                <h5 className="card-title">Style: {hat.style_name}</h5>
                <p className="card-text">
                  Location: {hat.location_closet_name}
                </p>
              </div>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">Color: {hat.color}</li>
                <li className="list-group-item">Fabric: {hat.fabric}</li>
              </ul>
              <div className="card-body">
                <button
                  className="btn btn-primary"
                  style={{ marginRight: "10px" }}
                  onClick={() => handleUpdate(hat.id)}
                >
                  Update
                </button>
                <button
                  className="btn btn-danger"
                  onClick={() => handleDelete(hat.id)}
                >
                  Delete Me
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default HatsList;
