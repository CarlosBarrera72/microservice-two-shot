import React, { useState, useEffect } from 'react';

const AddShoeForm = function() {
    const [formData, setFormData] = useState({
        model_name: '',
        manufacturer: '',
        color: '',
        picture_url: '',
        bin: '',
    });
    const [bins, setBins] = useState([]);

    const getBins = async function() {

        const url = 'http://localhost:8100/api/bins/';
        try {
            const res = await fetch(url);
    
            if (res.ok) {
                const { bins } = await res.json();
                setBins(bins);
            }
        } catch(e) {
            console.log('An error occurred fetching bins', e);
        }
    }

    useEffect(() => {
        getBins();
    }, [])

    const handleSubmit = async function(event) {
        event.preventDefault();
        const url = 'http://localhost:8080/api/shoes/';
        try {
            const fetchOptions = {
                method: 'post',
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json',
                }
            }
    
            const res = await fetch(url, fetchOptions);
            if (res.ok) {
                setFormData({
                    model_name: '',
                    manufacturer: '',
                    color: '',
                    picture_url: '',
                    bin: '',
                })
            }
        } catch(e) {
            console.log('An error occurred when attempting to POST a shoe', e);
        }

    }

    const handleFormChange = function({ target }) {
        const { value, name} = target;

        setFormData({
            ...formData,
            [name]: value
        });
    }

    const { 
        model_name:modelName,
        manufacturer, 
        color,
        picture_url:pictureUrl,
        bin,
    } = formData;

    return (
        <div>
            <h1>Add a Shoe to a Bin</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="model_name" className="form-label">Model Name:</label>
                    <input value={modelName} onChange={handleFormChange} type="text" className="form-control" name="model_name"></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="manufacturer" className="form-label">Manufacturer Name:</label>
                    <input value={manufacturer} onChange={handleFormChange} type="text" className="form-control" name="manufacturer"></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="color" className="form-label">Color:</label>
                    <input value={color} onChange={handleFormChange} type="text" className="form-control" name="color"></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="picture_url" className="form-label">Picture URL:</label>
                    <input value={pictureUrl} onChange={handleFormChange} type="text" className="form-control" name="picture_url"></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="bin" className="form-label">Bin</label>
                    <select value={bin} onChange={handleFormChange} className="form-control" name="bin">
                        <option value="">Select a bin</option>
                        {bins.map(({id, closet_name, bin_number}) => (
                            <option value={id} key={id + closet_name}>{closet_name} - Bin {bin_number}</option>
                        ))}
                    </select>
                </div>
                <button className="btn btn-lg btn-primary w-100" type="submit">Create Shoe</button>
            </form>
        </div>
    )
}

export default AddShoeForm;