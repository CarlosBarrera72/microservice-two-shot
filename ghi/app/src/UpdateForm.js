import React, { useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";

function HatForm() {
  const location = useLocation();
  const hatDtl = location.state?.hatData || {};

  const navigate = useNavigate();
  const [hatData, setHatData] = useState({
    id: hatDtl.id,
    fabric: hatDtl.fabric,
    style_name: hatDtl.style_name,
    color: hatDtl.color,
    picture_url: hatDtl.picture_url,
    location: hatDtl.location_href,
  });

  const [locations, setLocations] = useState([]);

  useEffect(() => {
    const fetchLocations = async () => {
      try {
        const response = await fetch("http://localhost:8100/api/locations/");
        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
        } else {
          console.error(response);
        }
      } catch (error) {
        console.error("Error loading locations:", error);
      }
    };

    fetchLocations();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setHatData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(
        `http://localhost:8090/api/hats/${hatData.id}/`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(hatData),
        }
      );

      if (response.ok) {
        setHatData({
          fabric: "",
          style_name: "",
          color: "",
          picture_url: "",
          location: "",
        });
        navigate("/hats");
      } else {
        console.error("Error updating hat");
      }
    } catch (error) {
      console.error("Error during hat addition:", error.message);
    }
  };

  return (
    <div className="container my-5">
      <div className="card shadow">
        <div className="card-body">
          <h1 className="card-title">Update Hat</h1>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label htmlFor="fabric" className="form-label">
                Fabric
              </label>
              <input
                type="text"
                className="form-control"
                id="fabric"
                name="fabric"
                value={hatData.fabric}
                onChange={handleChange}
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="style" className="form-label">
                Style
              </label>
              <input
                type="text"
                className="form-control"
                id="style"
                name="style_name"
                value={hatData.style_name}
                onChange={handleChange}
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="color" className="form-label">
                Color
              </label>
              <input
                type="text"
                className="form-control"
                id="color"
                name="color"
                value={hatData.color}
                onChange={handleChange}
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="picture_url" className="form-label">
                Picture URL
              </label>
              <input
                type="text"
                className="form-control"
                id="picture_url"
                name="picture_url"
                value={hatData.picture_url}
                onChange={handleChange}
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="location" className="form-label">
                Location
              </label>
              <select
                id="location"
                name="location"
                className="form-select"
                value={hatData.location}
                onChange={handleChange}
                required
              >
                <option value="" disabled>
                  Select a location
                </option>
                {locations.map((location) => (
                  <option key={location.id} value={location.id}>
                    {location.closet_name}
                  </option>
                ))}
              </select>
              <div className="mb-3">
                <label htmlFor="preview" className="form-label">
                  Picture Preview
                </label>
                <img
                  src={hatData.picture_url}
                  alt="Hat Preview"
                  className="img-fluid"
                  style={{ maxHeight: "200px" }}
                />
              </div>
            </div>
            <button type="submit" className="btn btn-primary">
              Update Hat
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
