import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO

# Import models from hats_rest, here.
# from hats.api.hats_rest.models import BinVo
# from hats_rest.models import Something

def poll():
    while True:
        # print('Hats poller polling for data')
        try:
            print('Hats poller polling for data')
            url = "http://wardrobe-api:8000/api/locations/"
            response = requests.get(url)
            print(response)
            content = json.loads(response.content)
            for location in content['locations']:
                LocationVO.objects.update_or_create(
                    import_href = location['href'],
                    defaults={
                        "closet_name": location['closet_name'],
                         "section_number" : location["section_number"],
                         "shelf_number" : location['shelf_number']
                          }
                )
                print("test")
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
