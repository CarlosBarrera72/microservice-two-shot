from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

from .models import Shoe, BinVO

# Create your views here.

class BinVOEncoder(ModelEncoder):
    model=BinVO
    properties=[
        "id",
        "import_href",
    ]

class ShoeDetailEncoder(ModelEncoder):
    model=Shoe
    properties=[
        "id",
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "bin"
    ]
    encoders={
        "bin": BinVOEncoder(),
    }



@require_http_methods(["GET", "POST"])
def list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(shoes, encoder=ShoeDetailEncoder, safe=False)
    else:
        # get information from the client on the request object
        # parse the JSON body of that request into a python dictionary
        content = json.loads(request.body)
        # get the corresponding BinVO for the bin passed by the client
        try:
            bin_href = f'/api/bins/{content["bin"]}/'
            bin_vo = BinVO.objects.get(import_href=bin_href)
        # assign that to a key in the dictionary
            content["bin"] = bin_vo
        except BinVO.DoesNotExist:
            message = f'No matching BinVO found for the bin with an id of {content["bin"]}'
            return JsonResponse({"message": message}, status=400)
        # use the python dictionary to create a new Shoe
        shoe = Shoe.objects.create(**content)
        # send back the new shoe to the client
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False, status=201)
        # probably gonna need the json parser
    
@require_http_methods(["DELETE"])
def detail_shoes(request, shoe_id = None):
    if request.method == "DELETE":
        deleted, _ = Shoe.objects.filter(id=shoe_id).delete()
        return JsonResponse({"deleted": deleted > 0})
